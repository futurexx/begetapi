import requests
import json


BASE_URL = ("https://api.beget.com/api/"
			"{section}/"
			"{function}/?"
			"login={login}&"
			"passwd={passwd}&"
			"input_format={input_format}&"
			"output_format={output_format}&"
			"input_data={input_data}"
			)

class BegetAPIBase(object):
    def __init__(self, api, function):
        self.api = api
        self.function = function


    def do_api_request(self, **kwargs):
        request_url = BASE_URL.format(
                section=self.api.section,
                function=self.function,
                login=self.api.auth.login,
                passwd=self.api.auth.passwd,
                input_format=kwargs['input_format'] if 'input_format' in kwargs.keys() else 'json',
                output_format=kwargs['output_format'] if 'output_format' in kwargs.keys() else 'json',
                input_data=requests.utils.requote_uri(json.dumps(kwargs))
                )
		response = json.loads(requests.post(request_url).content)

		return response


    def __call__(self, **kwargs):
        return do_api_request(**kwargs)



class BegetAPISection(object):

    def __init__(self, auth, section):
        self.auth = auth
        self.section = section


    def __getattr__(self, name):
        return BegetAPIBase(self, name)



class BegetAPI(object):

    def __init__(self, login, passwd):
        self.login = login
        self.passwd = passwd


    def __getattr__(self, name):
        return BegetAPISection(self, name)
